//
//  SpotifyService.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 3.11.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit
import NotificationBannerSwift

class SpotifyService: NSObject, SPTSessionManagerDelegate, SPTAppRemoteDelegate, SPTAppRemotePlayerStateDelegate {

    public static let sharedInstance = SpotifyService()
    
    private let SpotifyClientID = "cb84da3dfec44899bd50e43ab9b47c53"
    private let SpotifyRedirectURL = URL(string: "smartAmbientApp://spotify-login-callback")!
    private let tokenURL = "https://startambient.herokuapp.com/api/token"
    private let analysisURL = "https://api.spotify.com/v1/audio-analysis"
    private let incrementRate: CGFloat = 0.5
    private var playerTimer: Timer?
    private var loadingIndicator: SALoadingIndicator!
    private var isAlreadyConnected = false
    public var delegate: SpotifyPlayerDelegate? {
        didSet { delegate?.playerStateDidChange(player: currentPlayer) }
    }
    
    private lazy var configuration: SPTConfiguration = {
        let configuration = SPTConfiguration(clientID: SpotifyClientID, redirectURL: SpotifyRedirectURL)
        // Set the playURI to a non-nil value so that Spotify plays music after authenticating and App Remote can connect
        // otherwise another app switch will be required
        configuration.playURI = ""
        // Set these url's to your backend which contains the secret to exchange for an access token
        // You can use the provided ruby script spotify_token_swap.rb for testing purposes
        configuration.tokenSwapURL = URL(string: "https://startambient.herokuapp.com/api/token")
        configuration.tokenRefreshURL = URL(string: "https://startambient.herokuapp.com/api/refresh_token")
        return configuration
    }()
    
    public lazy var sessionManager: SPTSessionManager = {
        let manager = SPTSessionManager(configuration: configuration, delegate: self)
        return manager
    }()
    
    public lazy var appRemote: SPTAppRemote = {
        let appRemote = SPTAppRemote(configuration: configuration, logLevel: .info)
        appRemote.delegate = self
        return appRemote
    }()
    
    fileprivate var lastPlayerState: SPTAppRemotePlayerState?
    
    private override init() {
        super.init()
        self.loadingIndicator = SALoadingIndicator(maxRadii: 100)
        self.loadingIndicator.shouldShowBackground = true
        if let accToken = UserDefaults.standard.value(forKey: "spotify_accToken") as? String {
            self.appRemote.connectionParameters.accessToken = accToken
        }
    }
    
    public final func connectSpotify(with controller: UIViewController) {
        controller.view.addSubview(self.loadingIndicator)
        self.loadingIndicator.center = controller.view.center
        self.loadingIndicator.startAnimating()
        
        let scope: SPTScope = [.appRemoteControl, .playlistReadPrivate]
        sessionManager.alwaysShowAuthorizationDialog = false
        
        if #available(iOS 11, *) {
            // Use this on iOS 11 and above to take advantage of SFAuthenticationSession
            sessionManager.initiateSession(with: scope, options: .clientOnly)
        } else {
            // Use this on iOS versions < 11 to use SFSafariViewController
            sessionManager.initiateSession(with: scope, options: .clientOnly, presenting: controller)
        }
    }
    
    public final func open(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) {
        self.sessionManager.application(app, open: url, options: options)
//        guard let components = URLComponents(url: url, resolvingAgainstBaseURL: true),
//            let queryItems = components.queryItems else { return }
//
//        var code = ""
//        for item in queryItems {
//            if item.name == "code" { code = item.value! }
//        }
//
//        let url = "\(self.tokenURL)?grant_type=authorization_code&redirect_uri=\(self.SpotifyRedirectURL)&code=\(code)"
//        HTTPEngine.post(url, body: nil, headers: nil) { (error, result) in
//            guard let result = result as? [String:Any],
//            let accToken = result["access_token"] as? String else { return }
//
//            self.appRemote.connectionParameters.accessToken = accToken
//            self.appRemote.connect()
//        }
    }
    
    internal func sessionManager(manager: SPTSessionManager, didInitiate session: SPTSession) {
        // SUCCESS HANDLER
        LogInfo("Spotify Service Session Initiated Successfully")
        self.appRemote.connectionParameters.accessToken = session.accessToken
        UserDefaults.standard.set(session.accessToken, forKey: "spotify_accToken")
        self.appRemote.connect()
    }
    
    internal func sessionManager(manager: SPTSessionManager, didFailWith error: Error) {
        // FAIL HANDLER
        LogError("Spotify Service Session Failed - Error: \(error.localizedDescription)")
    }
    
    internal func appRemote(_ appRemote: SPTAppRemote, didFailConnectionAttemptWithError error: Error?) {
        // FAIL HANDLER
        let banner = NotificationBanner(title: "spotify_connection_failed_title".localized(), subtitle: nil, style: .danger)
        banner.duration = 1.0
        if let error = error {
            LogError("Spotify Service App Remote Connection Attemp Failed - Error: \(error.localizedDescription)")
            banner.subtitleLabel?.text = error.localizedDescription
        } else {
            LogError("Spotify Service App Remote Connection Attemp Failed")
        }
        banner.show()
        self.isAlreadyConnected = false
        if let delegate = delegate { delegate.playerStateDidChange(player: nil) }
        MusicPlayerViewModel.sharedInstance.shouldShow()
    }
    
    internal func appRemote(_ appRemote: SPTAppRemote, didDisconnectWithError error: Error?) {
        // DISCONNECT HANDLER
        let banner = NotificationBanner(title: "spotify_disconnected_title".localized(), subtitle: nil, style: .danger)
        banner.duration = 1.0
        if let error = error {
            LogError("Spotify Service App Remote Connection Disconnected with Error: \(error.localizedDescription)")
            banner.subtitleLabel?.text = error.localizedDescription
        } else {
            LogInfo("Spotify Service App Remote Connection Disconnected")
        }
        banner.show()
        self.isAlreadyConnected = false
        if let delegate = delegate { delegate.playerStateDidChange(player: nil) }
        MusicPlayerViewModel.sharedInstance.shouldHide()

    }
    
    internal func appRemoteDidEstablishConnection(_ appRemote: SPTAppRemote) {
        // SUCCESS HANDLER
        LogInfo("Spotify Service App Remote Connection established")
        self.appRemote.playerAPI?.delegate = self
        self.appRemote.playerAPI?.subscribe(toPlayerState: { (result, error) in
            if let error = error {
                LogError("Couldn't subscribe to Player API - Error: \(error.localizedDescription)")
                return
            }
            if !self.isAlreadyConnected {
                self.isAlreadyConnected = true
                let banner = NotificationBanner(title: "spotify_connected_title".localized(), subtitle: nil, style: .success)
                banner.duration = 1.0
                banner.show()
            }
        })
    }
    
    func playerStateDidChange(_ playerState: SPTAppRemotePlayerState) {
        self.currentPlayer = playerState
        self.getMusicAnalytics(for: playerState.track.uri, type: .beats)
        self.loadingIndicator.startAnimating()
        if let delegate = delegate { delegate.playerStateDidChange(player: playerState) }
        MusicPlayerViewModel.sharedInstance.shouldShow()
    }
    
    var currentPlayer: SPTAppRemotePlayerState? {
        didSet {
            self.startTimer()
        }
    }
    
    private func startTimer() {
        guard let playerState = currentPlayer else {
            return
        }
        if !playerState.isPaused {
            let duration = playerState.playbackPosition
            let speed = playerState.playbackSpeed
            self.startPlaying(fromDuration: duration, speed: speed, playerUri: playerState.track.uri)
        }
    }
    
    private func startPlaying(fromDuration duration: Int, speed: Float, playerUri: String) {
        self.currentDuration = CGFloat(duration)/1000.0
        self.currentDuration = self.currentDuration?.rounded(toPlaces: 1)
        if self.playerTimer != nil { self.playerTimer?.invalidate() }
        self.playerTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(self.incrementRate), repeats: true) { (timer) in
            self.currentDuration = self.currentDuration! + self.incrementRate
            self.checkTiming()
        }
    }
    
    private func checkTiming() {
        SpotifyService.sharedInstance.appRemote.playerAPI?.getPlayerState({ (result, error) in
            if let result = result as? SPTAppRemotePlayerState {
                let pos = result.playbackPosition/1000
                if (self.analyticsResult?[pos]) != nil && !(self.currentPlayer!.isPaused) {
                    self.loadingIndicator.stopAnimating()
                    let random = arc4random_uniform(6)
                    let color = self.colorArray[Int(random)]
                    HomeKit.instance.changeLampColors(with: color)
                    if let delegate = self.delegate { delegate.colorChanged(to: color) }
                }
            }
        })
    }
    
    private func startAmbient(keyPoint: CGFloat) {
        guard let val = self.analyticsResult![Int(keyPoint)] else { return }
        DispatchQueue.main.asyncAfter(deadline: .now() + TimeInterval(exactly: val)!) {
            
        }
    }
    
    public func pauseMusic(isSuccessBlock: @escaping (_ isSuccess: Bool)->()) {
        self.appRemote.playerAPI?.pause({ (result, error) in
            if let error = error {
                LogError("Couldn't pause music - Error: \(error.localizedDescription)")
                isSuccessBlock(false)
                return
            }
            isSuccessBlock(true)
        })
    }
    
    public func resumeMusic(isSuccessBlock: @escaping (_ isSuccess: Bool)->()) {
        self.appRemote.playerAPI?.resume({ (result, error) in
            if let error = error {
                LogError("Couldn't resume music - Error: \(error.localizedDescription)")
                isSuccessBlock(false)
                return
            }
            isSuccessBlock(true)
        })
    }
    
    public func nextMusic(isSuccessBlock: @escaping (_ isSuccess: Bool)->()) {
        self.appRemote.playerAPI?.skip(toNext: { (result, error) in
            DispatchQueue.global().async {
                if let error = error {
                    LogError("Couldn't skip to next music - Error: \(error.localizedDescription)")
                    isSuccessBlock(false)
                    return
                }
                isSuccessBlock(true)
            }
            
        })
    }
    
    public func previousMusic(isSuccessBlock: @escaping (_ isSuccess: Bool)->()) {
        self.appRemote.playerAPI?.skip(toPrevious: { (result, error) in
            if let error = error {
                LogError("Couldn't skip to previous music - Error: \(error.localizedDescription)")
                isSuccessBlock(false)
                return
            }
            isSuccessBlock(true)
        })
    }
    
    public func getImage(track: SPTAppRemoteTrack, completion: @escaping (_ image: UIImage?)->()) {
        SpotifyService.sharedInstance.appRemote.imageAPI?.fetchImage(forItem: track, with: CGSize(width: 600, height: 600), callback: { (image, error) in
            if let error = error {
                LogError("Couldn't fetch image for track with name: \(track.name) - Error: \(error.localizedDescription)")
                completion(nil)
                return
            }
            completion(image as? UIImage)
        })
    }
    
    func getMusicAnalytics(for uri: String, type: SpotifyAnalyticType = .beats) {
        guard let accessToken = self.appRemote.connectionParameters.accessToken else {
            return
        }
        let parts = uri.components(separatedBy: ":")
        if parts.count < 3 {
            return
        }
        
        let id = parts[2]
        let url = "\(self.analysisURL)/\(id)"
        let header = [
            "Authorization": "Bearer \(accessToken)"
        ]
        HTTPEngine.get(url, headers: header) { (error, result) in
            if let error = error {
                LogError("Couldn't get analytics for Spotify Music with ID: \(id) - Error: \(error.localizedDescription)")
                return
            }
            
            guard let result = result as? [String:Any],
            let dict = result[type.rawValue] as? [[String:Any]] else {
                    LogError("Couldn't get result body matching for Spotify Analytics for ID: \(id)")
                    return
            }
            
            self.analyticsResult = nil
            self.analyticsResult = [Int:CGFloat]()
            for item in dict {
                if var start = item["start"] as? CGFloat,
                    let duration = item["duration"] as? CGFloat {
                    start = start.rounded(toPlaces: 1)
                    self.analyticsResult![Int(start)] = duration
                }
//                if let analytic = SpotifyAnalytics(dict: item) {
//                    self.analyticsResult?.append(analytic)
//                }
            }
            self.currentAnalyticUri = self.currentPlayer?.track.uri
        }
    }
    
    var analyticsResult: [Int:CGFloat]?
    var currentAnalyticUri: String?
    var currentDuration: CGFloat?
    lazy var colorArray = [
        UIColor.red,
        UIColor.blue,
        UIColor.green,
        UIColor.orange,
        UIColor.yellow,
        UIColor.magenta,
        UIColor.cyan
    ]
}

public enum SpotifyAnalyticType: String {
    case bars = "bars"
    case beats = "beats"
    case tatums = "tatums"
}


protocol SpotifyPlayerDelegate {
    func playerStateDidChange(player: SPTAppRemotePlayerState?)
    func colorChanged(to color: UIColor?)
}
