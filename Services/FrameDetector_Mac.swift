//
//  FrameDetector_Mac.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 21.10.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import Vision
import Cocoa

class FrameDetector {
    
    public var confidenceLayer: CAShapeLayer?
    public var pathLayer: CALayer?
    
    init() {
        
    }
    
    public func capture(image: NSImage) {
        if SAConfiguration.sharedInstance.shouldAutoDetect {
            var imageRect:CGRect = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
            
            guard let cgImage = image.cgImage(forProposedRect: &imageRect, context: nil, hints: nil),
//                let _ = UInt32(exactly: image.imageOrientation.rawValue),
                let cgOrientation = CGImagePropertyOrientation(rawValue: 0) else { return }
            
            self.performVisionRequest(image: cgImage, orientation: cgOrientation)
        }
    }
    
    @available(iOS 11.0, *)
    lazy var rectangleDetectionRequest: VNDetectRectanglesRequest = {
        let rectDetectRequest = VNDetectRectanglesRequest(completionHandler: self.handleDetectedRectangles)
        // Customize & configure the request to detect only certain rectangles.
        rectDetectRequest.maximumObservations = 8 // Vision currently supports up to 16.
        rectDetectRequest.minimumConfidence = 0.7 // Be confident.
        rectDetectRequest.minimumAspectRatio = 0.52 // height / width
        rectDetectRequest.maximumAspectRatio = 0.75
        return rectDetectRequest
    }()
    
    fileprivate func performVisionRequest(image: CGImage, orientation: CGImagePropertyOrientation) {
        if #available(iOS 11, *) {
            let requests = createVisionRequests()
            // Create a request handler.
            let imageRequestHandler = VNImageRequestHandler(cgImage: image,
                                                            orientation: orientation,
                                                            options: [:])
            
            // Send the requests to the request handler.
            DispatchQueue.global(qos: .userInitiated).async {
                do {
                    try imageRequestHandler.perform(requests)
                } catch let error as NSError {
                    LogError("Failed to perform image request: \(error)")
                    return
                }
            }
        }
        // Fetch desired requests based on switch status.
        
    }
    
    @available(iOS 11.0, *)
    fileprivate func createVisionRequests() -> [VNRequest] {
        
        // Create an array to collect all desired requests.
        var requests: [VNRequest] = []
        
        // Create & include a request if and only if switch is ON.
        requests.append(self.rectangleDetectionRequest)
        
        // Return grouped requests as a single array.
        return requests
    }
    
    @available(iOS 11.0, *)
    fileprivate func handleDetectedRectangles(request: VNRequest?, error: Error?) {
        if let nsError = error as NSError? {
            LogError("Couldn't handle detected rectangle - Error: \(nsError.localizedDescription)")
            return
        }
        // Since handlers are executing on a background thread, explicitly send draw calls to the main thread.
        DispatchQueue.main.async {
            guard let drawLayer = self.pathLayer,
                let results = request?.results as? [VNRectangleObservation] else {
                    return
            }
            if self.isTVDetected(rect: drawLayer.bounds) {
                self.draw(rectangles: results, onImageWithBounds: drawLayer.bounds)
                drawLayer.setNeedsDisplay()
            }
        }
    }
    
    fileprivate func isTVDetected(rect: CGRect) -> Bool {
        let width = rect.width
        let height = rect.height
        let ratio = height/width
        
        if ratio >= wideTVRatio-ratioEstimation && ratio <= wideTVRatio+ratioEstimation {
            LogInfo("Wide Screen TV Detected")
            return true
        }
        
        if ratio >= squareTVRatio-ratioEstimation && ratio <= squareTVRatio+ratioEstimation {
            LogInfo("Square TV Detected")
            return true
        }
        
        if ratio >= cinemaTVRatio-ratioEstimation && ratio <= cinemaTVRatio+ratioEstimation {
            LogInfo("Cinema Screen Detected")
            return true
        }
        
        return false
    }
    
    private var squareTVRatio: CGFloat = 3/4
    private var wideTVRatio: CGFloat = 9/16
    private var cinemaTVRatio: CGFloat = 9/21
    private var ratioEstimation: CGFloat = 0.21
    
    @available(iOS 11.0, *)
    fileprivate func draw(rectangles: [VNRectangleObservation], onImageWithBounds bounds: CGRect) {
        CATransaction.begin()
        for observation in rectangles {
            let rectBox = boundingBox(forRegionOfInterest: observation.boundingBox, withinImageBounds: bounds)
            self.confidenceLayer?.removeFromSuperlayer()
            self.removableTimer?.invalidate()
            self.confidenceLayer = shapeLayer(color: .red, frame: rectBox)
            
            guard let pathLayer = self.pathLayer else { return }
            pathLayer.addSublayer(self.confidenceLayer!)
            
            self.removableTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(CGFloat(SAConfiguration.sharedInstance.changeInterval)*3), repeats: false, block: { (timer) in
                self.confidenceLayer?.removeFromSuperlayer()
                self.confidenceLayer = nil
            })
        }
        CATransaction.commit()
    }
    
    var removableTimer: Timer?
    
    fileprivate func boundingBox(forRegionOfInterest: CGRect, withinImageBounds bounds: CGRect) -> CGRect {
        
        let imageWidth = bounds.width
        let imageHeight = bounds.height
        
        // Begin with input rect.
        var rect = forRegionOfInterest
        
        // Reposition origin.
        rect.origin.x *= imageWidth
        rect.origin.x += bounds.origin.x
        rect.origin.y = (1 - rect.origin.y) * imageHeight + bounds.origin.y
        
        // Rescale normalized coordinates.
        rect.size.width *= imageWidth
        rect.size.height *= imageHeight
        
        return rect
    }
    
    fileprivate func shapeLayer(color: NSColor, frame: CGRect) -> CAShapeLayer {
        // Create a new layer.
        let layer = CAShapeLayer()
        
        // Configure layer's appearance.
        layer.fillColor = nil // No fill to show boxed object
        layer.shadowOpacity = 0
        layer.shadowRadius = 0
        layer.borderWidth = SAConfiguration.sharedInstance.shouldShowGuides ? 2 : 0
        
        // Vary the line color according to input.
        layer.borderColor = color.cgColor
        
        // Locate the layer.
        layer.anchorPoint = .zero
        layer.frame = frame
        layer.masksToBounds = true
        
        // Transform the layer to have same coordinate system as the imageView underneath it.
        layer.transform = CATransform3DMakeScale(1, -1, 1)
        
        return layer
    }
    
}
