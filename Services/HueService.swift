//
//  HueService.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 21.10.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import Cocoa

class HueService: NSObject {
    
    public static let sharedInstance = HueService()
    
    private let discoveryUrl: String = "https://discovery.meethue.com"
    
    private var internalIP: String?
    private var internalID: String?
    private var username: String? { // 5jl8PCI17SMQx9tohihwDJ2QsAmXTwChbVdA5RTp
        didSet {
            if username != nil {
                self.isUsernameSet = true
            }
        }
    }
    
    public var isUsernameSet: Bool = false
    
    public var lightIDs = [String]()
    
    private override init() {
        super.init()
        username = "5jl8PCI17SMQx9tohihwDJ2QsAmXTwChbVdA5RTp"
        if let username = username {
            self.getIPAddress { (ip) in
                self.getAllLights()
            }
        } else {
            self.checkConnection()
        }
    }
    
    public func start() {
        
    }
    
    func getIPAddress(completion: ((_ ipAddress: String?)->())? = nil) {
        HTTPEngine.get(discoveryUrl) { (error, response) in
            if let error = error {
                LogError("Couldn't get IP Address - Error: \(error.localizedDescription)")
                return
            }
            
            guard let response = response as? [NSDictionary],
                let firstElement = response.first,
                let id = firstElement["id"] as? String,
                let ipAddress = firstElement["internalipaddress"] as? String else {
                    LogError("Couldn't find IP Address")
                    return
            }
            
            self.internalIP = ipAddress
            self.internalID = id
            completion?(ipAddress)
        }
    }
    
    func checkConnection(completion: ((_ username: String?)->())? = nil) {
        self.getIPAddress { (ip) in
            guard let ip = ip else {
                return
            }
            
            let url = "http://\(ip)/api"
            let body = [
                "devicetype": "Smart_Ambient_App"
            ]
            HTTPEngine.post(url, body: body, headers: nil, completion: { (error, result) in
                if let error = error {
                    LogError("Couldn't check connection - Error: \(error.localizedDescription)")
                    return
                }
                
                /*
                 [
                    {
                        "success": {
                            "username": "cM1GK-naFjq13PmO9TdRpihgcOf-KvPPQhONkDx3"
                        }
                    }
                 ]
                 */
                if let result = result as? [NSDictionary],
                    let firstElement = result.first,
                    let key = firstElement.allKeys.first as? String {
                    if key == "error" {
                        let description = firstElement.value(forKey: key)
                        // Link Button Not pressed
                    } else if key == "success" {
                        guard let usernameDict = firstElement.value(forKey: key) as? [String:String],
                            let username = usernameDict["username"] else {
                                return
                        }
                        
                        self.username = username
                        completion?(username)
                    }
                }
            })
        }
    }
    
    private func getAllLights() {
        guard let ip = self.internalIP,
            let username = self.username else {
                return
        }
        let url = "http://\(ip)/api/\(username)/lights"
        
        HTTPEngine.get(url) { (error, response) in
            if let error = error {
                LogError("Error occured during getting all lights - Error: \(error.localizedDescription)")
                return
            }
            /*
 "{\n    1 =     {\n        capabilities =         {\n            certified = 1;\n            control =             {\n                colorgamut =                 (\n                                        (\n                        \"0.6915\",\n                        \"0.3083\"\n                    ),\n                                        (\n                        \"0.17\",\n                        \"0.7\"\n                    ),\n                                        (\n                        \"0.1532\",\n                        \"0.0475\"\n                    )\n                );\n                colorgamuttype = C;\n                ct =                 {\n                    max = 500;\n                    min = 153;\n                };\n                maxlumen = 806;\n                mindimlevel = 1000;\n            };\n            streaming =             {\n                proxy = 1;\n                renderer = 1;\n            };\n        };\n        config =         {\n            archetype = sultanbulb;\n            direction = omnidirectional;\n            function = mixed;\n        };\n        manufacturername = Philips;\n        modelid = LCT015;\n        name = L2;\n        productid = \"Philips-LCT015-1-A19ECLv5\";\n        productname = \"Hue color lamp\";\n        state =         {\n            alert = none;\n            bri = 254;\n            colormode = ct;\n            ct = 366;\n            effect = none;\n            hue = 8418;\n            mode = homeautomation;\n            on = 1;\n            reachable = 0;\n            sat = 140;\n            xy =             (\n                \"0.4573\",\n                \"0.41\"\n            );\n        };\n        swconfigid = 3416C2DD;\n        swupdate =         {\n            lastinstall = \"1980-01-01T00:00:08\";\n            state = noupdates;\n        };\n        swversion = \"1.29.0_r21169\";\n        type = \"Extended color light\";\n        uniqueid = \"00:17:88:01:03:12:61:13-0b\";\n    };\n    2 =     {\n        capabilities =         {\n            certified = 1;\n            control =             {\n                colorgamut =                 (\n                                        (\n                        \"0.6915\",\n                        \"0.3083\"\n                    ),\n                                        (\n                        \"0.17\",\n                        \"0.7\"\n                    ),\n                                        (\n                        \"0.1532\",\n                        \"0.0475\"\n                    )\n                );\n                colorgamuttype = C;\n                ct =                 {\n                    max = 500;\n                    min = 153;\n                };\n                maxlumen = 806;\n                mindimlevel = 1000;\n            };\n            streaming =             {\n                proxy = 1;\n                renderer = 1;\n            };\n        };\n        config =         {\n            archetype = sultanbulb;\n            direction = omnidirectional;\n            function = mixed;\n        };\n        manufacturername = Philips;\n        modelid = LCT015;\n        name = L1;\n        productid = \"Philips-LCT015-1-A19ECLv5\";\n        productname = \"Hue color lamp\";\n        state =         {\n            alert = none;\n            bri = 254;\n            colormode = ct;\n            ct = 366;\n            effect = none;\n            hue = 8418;\n            mode = homeautomation;\n            on = 0;\n            reachable = 0;\n            sat = 140;\n            xy =             (\n                \"0.4573\",\n                \"0.41\"\n            );\n        };\n        swconfigid = 3416C2DD;\n        swupdate =         {\n            lastinstall = \"<null>\";\n            state = noupdates;\n        };\n        swversion = \"1.29.0_r21169\";\n        type = \"Extended color light\";\n        uniqueid = \"00:17:88:01:03:10:77:25-0b\";\n    };\n    3 =     {\n        capabilities =         {\n            certified = 1;\n            control =             {\n                colorgamut =                 (\n                                        (\n                        \"0.6915\",\n                        \"0.3083\"\n                    ),\n                                        (\n                        \"0.17\",\n                        \"0.7\"\n                    ),\n                                        (\n                        \"0.1532\",\n                        \"0.0475\"\n                    )\n                );\n                colorgamuttype = C;\n                ct =                 {\n                    max = 500;\n                    min = 153;\n                };\n                maxlumen = 806;\n                mindimlevel = 1000;\n            };\n            streaming =             {\n                proxy = 1;\n                renderer = 1;\n            };\n        };\n        config =         {\n            archetype = sultanbulb;\n            direction = omnidirectional;\n            function = mixed;\n        };\n        manufacturername = Philips;\n        modelid = LCT015;\n        name = R1;\n        productid = \"Philips-LCT015-1-A19ECLv5\";\n        productname = \"Hue color lamp\";\n        state =         {\n            alert = none;\n            bri = 254;\n            colormode = ct;\n            ct = 366;\n            effect = none;\n            hue = 8418;\n            mode = homeautomation;\n            on = 0;\n            reachable = 0;\n            sat = 140;\n            xy =             (\n                \"0.4573\",\n                \"0.41\"\n            );\n        };\n        swconfigid = 3416C2DD;\n        swupdate =         {\n            lastinstall = \"2018-02-15T17:57:52\";\n            state = noupdates;\n        };\n        swversion = \"1.29.0_r21169\";\n        type = \"Extended color light\";\n        uniqueid = \"00:17:88:01:03:12:3b:11-0b\";\n    };\n}"
 
    */
            guard let response = response as? NSDictionary,
                let keyList = response.allKeys as? [String] else {
                    return
            }
            
            // Should get all IDs of lights
            self.lightIDs = keyList
            self.setColorToAllLights(color: NSColor.blue)
        }
    }
    
    public func setColorToAllLights(color: NSColor) {
        for id in self.lightIDs {
            self.setColorToLight(id: id, color: color) { (error) in
                if let error = error {
                    LogError("Couldn't set color for light with ID: \(id) - Color: \(color.description) - Error: \(error.localizedDescription)")
                    return
                }
                
                LogInfo("Color is set to light with ID: \(id) - Color: \(color.description)")
            }
        }
    }
    
    public func setColorToLight(id: String, color: NSColor, completion: ((_ error: NSError?)->())?) {
        let hsba = color.hsba(from: color)
        let hue = Int(hsba[0]*65535)
        
        var brightness = Int(hsba[1]*254)
        if brightness <= 0 {
            brightness = 1
        } else if brightness >= 254 {
            brightness = 254
        }
        
        var saturation = Int(hsba[2]*254)
        if saturation <= 0 {
            saturation = 1
        } else if saturation >= 254 {
            saturation = 254
        }
        
        let body = [
            lightAttributes.hue: hue,
            lightAttributes.brightness: brightness,
            lightAttributes.saturation: saturation,
            lightAttributes.power: (color.isBlackish()) ? false : true
            ] as [String : Any]
        
        self.setAttributes(for: id, attributes: body, completion: completion)
        
    }
    
    public func setAttributes(for id: String, attributes: [String:Any], completion: ((_ error: NSError?)->())?) {
        guard let ip = self.internalIP,
            let username = self.username else {
                return
        }
        let url = "http://\(ip)/api/\(username)/lights/\(id)/state"
        
        HTTPEngine.put(url, body: attributes, headers: nil) { (error, response) in
            if let error = error {
                completion?(error as NSError)
                return
            }
            
            guard let response = response else {
                return
            }
            
            print(response)
            completion?(nil)
        }
        
    }
    
    public enum lightAttributes {
        
        /// Boolean
        static let power = "on"
        
        /// [1-254]
        static let brightness = "bri"
        
        /// [0-65535] 0&65535->RED 25500->GREEN 46920->BLUE
        static let hue = "hue"
        
        /// [1-254] 254->Saturated(Colored) 0->White
        static let saturation = "sat"
    }
    
}
