//
//  SAConfiguration.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 30.09.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import Foundation

class SAConfiguration: NSObject {
    
    // MARK: - Public Properties
    public static let sharedInstance = SAConfiguration()
    
    public var isDebugMode: Bool
    public var colorMode: ColorMode! { didSet { if oldValue != self.colorMode { self.shouldReload = true } } }
    public var changeInterval: Int! { didSet { if oldValue != self.changeInterval { self.shouldReload = true } } }
    public var shouldAutoDetect: Bool { didSet { if oldValue != self.shouldAutoDetect { self.shouldReload = true } } }
    public var shouldShowGuides: Bool { didSet { if oldValue != self.shouldShowGuides { self.shouldReload = true } } }
    public var shouldCloseWhenBlack: Bool { didSet { if oldValue != self.shouldCloseWhenBlack { self.shouldReload = true } } }

    public var isAnimationPlaying: Bool = false
    public var frameSmoothness: Float = 0.5
    public var appLongVersion: String { get { return self.appVersion() + "." + self.appBuild() } }
    public private(set) var shouldReload: Bool! { didSet { if shouldReload { self.configurationChangeBlock?() } } }
    
    typealias ConfigurationChangeNotifierBlock = ()->()
    
    // MARK: - Private Properties
    private var configurationChangeBlock: ConfigurationChangeNotifierBlock?
    private var stopNotifierBlock: ConfigurationChangeNotifierBlock?

    override private init() {
        // Default Configurations
        self.isDebugMode = false
        self.colorMode = .background
        self.changeInterval = 1
        self.shouldReload = true
        self.shouldAutoDetect = true
        self.shouldShowGuides = true
        self.shouldCloseWhenBlack = true
    }
    
    // MARK: - Public Methods
    
    public func save() {
        let defaults = UserDefaults.standard
        defaults.set(colorMode.rawValue, forKey: "configuration_colorMode")
        defaults.set(changeInterval, forKey: "configuration_changeInterval")
        defaults.set(frameSmoothness, forKey: "configuration_frameSmoothness")
    }
    
    public func start() {
        let defaults = UserDefaults.standard
        if let val = defaults.value(forKey: "configuration_colorMode") as? Int { colorMode = ColorMode(rawValue: val) }
        if let val = defaults.value(forKey: "configuration_changeInterval") as? Int { changeInterval = val }
        if let val = defaults.value(forKey: "configuration_frameSmoothness") as? Float { frameSmoothness = val }
    }
    
    public func setConfigurationNotifier(_ notifierBlock: @escaping ConfigurationChangeNotifierBlock) {
        self.configurationChangeBlock = notifierBlock
    }
    
    public func setStopAnimationRequest(_ notifierBlock: @escaping ConfigurationChangeNotifierBlock) {
        self.stopNotifierBlock = notifierBlock
    }
    
    public func sendStopAnimationRequest() {
        self.stopNotifierBlock?()
    }
    
    public func reloaded() {
        self.shouldReload = false
    }
    
    
    private func appVersion() -> String {
        return (Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String) ?? ""
    }
    
    private func appBuild() -> String {
        return (Bundle.main.infoDictionary!["CFBundleVersion"] as? String) ?? ""
    }

    
}

enum ColorMode: Int {
    case primary = 0
    case detail = 1
    case background = 2
}
