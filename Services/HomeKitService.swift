//
//  HomeKitService.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 30.09.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit
import HomeKit
import NotificationBannerSwift

public class HomeKit: NSObject, HMHomeManagerDelegate {
    
    public static let instance = HomeKit()
    
    private var homeManager: HMHomeManager!
    public private(set) var accessories = Set<HMAccessory>()
    private var maxBrightness = 1.0
    private var errorCount = 0 {
        didSet { self.showError() }
    }
    
    private let maxErrorCount = 5
    private var isErrorShown = false
    
    private override init() {
        super.init()
        self.homeManager = HMHomeManager()
        self.homeManager.delegate = self
    }
    
    public func prepareLamps(with image: UIImage) {
        let colors = image.getColors()
        self.changeLampColors(with: colors.background)
    }
    
    private func showError() {
        if errorCount >= self.maxErrorCount && !self.isErrorShown {
            let banner = NotificationBanner(title: "lamp_adjust_error_title".localized(), style: .danger)
            banner.show()
            self.isErrorShown = true
            Timer.scheduledTimer(withTimeInterval: 30, repeats: false) { (timer) in
                self.errorCount = 0
                self.isErrorShown = false
            }
        }        
    }
    
    private func getAllLamps() {
        guard let home = self.homeManager.primaryHome else { return }
        for room in home.rooms {
            for accessory in room.accessories {
                for service in accessory.services {
                    for characteristic in service.characteristics {
                        if characteristic.characteristicType == HMCharacteristicTypeBrightness {
                            self.accessories.insert(accessory)
                        }
                    }
                }
            }
        }
    }
    
    public func changeLampColor(accessory: HMAccessory, color: UIColor) {
        let hsbaColor = color.hsba()
        for service in accessory.services {
            for characteristic in service.characteristics {
                if characteristic.characteristicType == HMCharacteristicTypeBrightness {
                    let max = characteristic.metadata?.maximumValue?.floatValue ?? 1.0
                    let value = NSNumber(value: Float(hsbaColor[2]) * max)
                    characteristic.writeValue(Int(max)) { (error) in
                        if let error = error {
                            LogError("Couldn't set brightness to characteristic - Error: \(error.localizedDescription) - Value: \(max) - Max: \(max)")
                            self.errorCount = self.errorCount + 1
                            return
                        }
                        
                        LogInfo("Brightness value is changed successfully - Value: \(value) - Max: \(max)")
                    }
                } else if characteristic.characteristicType == HMCharacteristicTypeHue {
                    let max = characteristic.metadata?.maximumValue?.floatValue ?? 1.0
                    let value = NSNumber(value: Float(hsbaColor[0]) * max)
                    characteristic.writeValue(value) { (error) in
                        if let error = error {
                            LogError("Couldn't set hue to characteristic - Error: \(error.localizedDescription) - Value: \(value) - Max: \(max)")
                            self.errorCount = self.errorCount + 1
                            return
                        }
                        
                        LogInfo("Hue value is changed successfully - Value: \(value) - Max: \(max)")
                    }
                } else if characteristic.characteristicType == HMCharacteristicTypeSaturation {
                    let max = characteristic.metadata?.maximumValue?.floatValue ?? 1.0
                    let value = NSNumber(value: Float(hsbaColor[1]) * max)
                    characteristic.writeValue(value) { (error) in
                        if let error = error {
                            LogError("Couldn't set saturation to characteristic - Error: \(error.localizedDescription) - Value: \(value) - Max: \(max)")
                            self.errorCount = self.errorCount + 1
                            return
                        }
                        
                        LogInfo("Saturation value is changed successfully - Value: \(value) - Max: \(max)")
                    }
                } else if characteristic.characteristicType == HMCharacteristicTypePowerState {
                    characteristic.writeValue(SAConfiguration.sharedInstance.shouldCloseWhenBlack ? color.isBlackish() ? false : true : true ) { (error) in
                        if let error = error {
                            LogError("Couldn't set power to characteristic - Error: \(error.localizedDescription)")
                            self.errorCount = self.errorCount + 1
                            return
                        }
                        
                        LogInfo("Power value is changed successfully")
                    }
                } else if characteristic.characteristicType == HMCharacteristicTypeCurrentLightLevel {
                    let max = characteristic.metadata?.maximumValue?.floatValue ?? 1.0
                    let value = NSNumber(value: 1 * max)
                    characteristic.writeValue(value) { (error) in
                        if let error = error {
                            LogError("Couldn't set light level to characteristic - Error: \(error.localizedDescription) - Value: \(value) - Max: \(max)")
                            self.errorCount = self.errorCount + 1
                            return
                        }
                        
                        LogInfo("Light level value is changed successfully - Value: \(value) - Max: \(max)")
                    }
                }
            }
        }
    }
    
    public func changeLampColors(with color: UIColor) {
        for accessory in self.accessories {
            self.changeLampColor(accessory: accessory, color: color)
        }
    }
    
    public func homeManagerDidUpdateHomes(_ manager: HMHomeManager) {
        self.getAllLamps()
    }
    
    public func homeManagerDidUpdatePrimaryHome(_ manager: HMHomeManager) {
        self.getAllLamps()
    }
    
    public func homeManager(_ manager: HMHomeManager, didAdd home: HMHome) {
        self.getAllLamps()
    }
    
    public func homeManager(_ manager: HMHomeManager, didRemove home: HMHome) {
        self.getAllLamps()
    }
    
   
    
}


