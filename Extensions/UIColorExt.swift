//
//  UIColorExt.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 30.09.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

extension UIColor {
    public func isBlackish() -> Bool {
        var red: CGFloat = 0
        var green: CGFloat = 0
        var blue: CGFloat = 0
        self.getRed(&red, green: &green, blue: &blue, alpha: nil)
        return (red+green+blue >= 0.3) ? false : true
    }
    
    public func hsba() -> [CGFloat] {
        var HSBA = [CGFloat](repeating: 0.0, count: 4)
        var hue: CGFloat = 0.0
        var saturation: CGFloat = 0.0
        var brightness: CGFloat = 0.0
        var alpha: CGFloat = 0.0
        self.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha)
        
        HSBA = [hue, saturation, brightness, alpha]
        return HSBA
    }
}
