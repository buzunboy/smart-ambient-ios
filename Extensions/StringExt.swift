//
//  StringExt.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 30.09.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import Foundation

extension String {
    
    public func localized() -> String {
        return NSLocalizedString(self, comment: "")
    }
    
}
