//
//  CGFloatExt.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 4.11.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

extension CGFloat {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> CGFloat {
        let divisor = pow(10.0, CGFloat(places))
        return (self * divisor).rounded() / divisor
    }
}
