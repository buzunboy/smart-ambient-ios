//
//  NSNotificationExt.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 6.11.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import Foundation

extension NSNotification.Name {
    
    static let playerStateDidChange = Notification.Name(rawValue: "playerStateDidChange")
    
}
