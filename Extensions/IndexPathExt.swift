//
//  IndexPathExt.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 14.11.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

extension IndexPath {
    
    struct HomeTable {
        static let cameraAmbient = IndexPath(row: 0, section: 0)
        static let musicAmbient = IndexPath(row: 1, section: 0)
        static let colorPicker = IndexPath(row: 2, section: 0)
    }
    
}
