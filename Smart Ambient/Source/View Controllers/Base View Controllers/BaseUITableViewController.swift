//
//  BaseUITableViewController.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 2.10.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class BaseUITableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
}
