//
//  SettingsTableViewController.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 30.09.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class SettingsTableViewController: BaseUITableViewController {
    
    private var pickerView: UIPickerView!
    private var hiddenTF: UITextField!
    private var aboutVC: UINavigationController?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.frame.size.height = self.tableView.contentSize.height
        self.preferredContentSize.height = self.tableView.contentSize.height
        self.popoverPresentationController?.presentedView?.frame.size.height = self.tableView.contentSize.height
        UIView.animate(withDuration: 0.5) {
            self.view.alpha = 1.0
            self.popoverPresentationController?.presentedView?.alpha = 1.0
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.hideViews()
    }
    
    func prepareUI() {
        self.hiddenTF = UITextField(frame: CGRect(x: 0, y: 0, width: 1, height: 1))
        self.hiddenTF.alpha = 0.0
        self.view.addSubview(self.hiddenTF)
        
        if #available(iOS 11, *) {
            self.pickerView = UIPickerView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height - (UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0) - 250, width: UIScreen.main.bounds.width, height: 250))
        } else {
            self.pickerView = UIPickerView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height - 250, width: UIScreen.main.bounds.width, height: 250))
        }
        
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.pickerView.backgroundColor = .white
        
        
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneClicked(sender:)))
        let cancelBtn = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelClicked(sender:)))
        let flexBtn = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 300, height: 44))
        toolbar.barStyle = .default
        toolbar.setItems([cancelBtn, flexBtn, doneBtn], animated: true)
        
        self.hiddenTF.inputAccessoryView = toolbar
        self.hiddenTF.inputView = self.pickerView
    }
    
    func hideViews() {
        self.view.alpha = 0.0
        self.popoverPresentationController?.presentedView?.alpha = 0.0
    }
    
    @objc func doneClicked(sender: Any? = nil) {
        let row = self.pickerView.selectedRow(inComponent: 0)
        
        if self.pickerView.tag == pickerTags.changeInterval {
            SAConfiguration.sharedInstance.changeInterval = self.changeIntervalArray[row]
        }
        else if self.pickerView.tag == pickerTags.colorChooser {
            SAConfiguration.sharedInstance.colorMode = ColorMode(rawValue: row)
        }
        
        self.tableView.reloadData()
        self.hiddenTF.resignFirstResponder()
    }
    
    @objc func cancelClicked(sender: Any? = nil) {
        self.hiddenTF.resignFirstResponder()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 { return 6 }
        else if section == 1 { return 1 }
        
        return 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath == tableIndexPaths.changeInterval {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsStandardTableViewCell", for: indexPath)
            cell.textLabel?.text = "settings_timeInterval_title".localized()
            cell.detailTextLabel?.text = String(describing: SAConfiguration.sharedInstance.changeInterval!) + "time_sec".localized()
            cell.textLabel?.font = UIFont.systemFont(ofSize: 17)
            cell.textLabel?.textColor = .black
            return cell
        }
        
        else if indexPath == tableIndexPaths.colorChooser {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsStandardTableViewCell", for: indexPath)
            cell.textLabel?.text = "settings_color_title".localized()
            cell.detailTextLabel?.text = String(describing: SAConfiguration.sharedInstance.colorMode!).localized()
            cell.textLabel?.font = UIFont.systemFont(ofSize: 17)
            cell.textLabel?.textColor = .black
            return cell
        }
            
        else if indexPath == tableIndexPaths.autoDetect {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsSwitchTableViewCell", for: indexPath) as! SettingsSwitchTableViewCell
            cell.titleLabel?.text = "settings_autoDetect_title".localized()
            cell.switchBtn.isOn = SAConfiguration.sharedInstance.shouldAutoDetect
            cell.switchValueChanged { (isOn) in
                SAConfiguration.sharedInstance.shouldAutoDetect = isOn
            }
            cell.textLabel?.font = UIFont.systemFont(ofSize: 17)
            cell.textLabel?.textColor = .black
            return cell
        }
            
        else if indexPath == tableIndexPaths.showGuides {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsSwitchTableViewCell", for: indexPath) as! SettingsSwitchTableViewCell
            cell.titleLabel?.text = "settings_showGuides_title".localized()
            cell.switchBtn.isOn = SAConfiguration.sharedInstance.shouldShowGuides
            cell.switchValueChanged { (isOn) in
                SAConfiguration.sharedInstance.shouldShowGuides = isOn
            }
            cell.textLabel?.font = UIFont.systemFont(ofSize: 17)
            cell.textLabel?.textColor = .black
            return cell
        }
            
        else if indexPath == tableIndexPaths.blackDetection {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsSwitchTableViewCell", for: indexPath) as! SettingsSwitchTableViewCell
            cell.titleLabel?.text = "settings_closeWhenBlack_title".localized()
            cell.switchBtn.isOn = SAConfiguration.sharedInstance.shouldCloseWhenBlack
            cell.switchValueChanged { (isOn) in
                SAConfiguration.sharedInstance.shouldCloseWhenBlack = isOn
            }
            cell.textLabel?.font = UIFont.systemFont(ofSize: 17)
            cell.textLabel?.textColor = .black
            return cell
        }
        
        else if indexPath == tableIndexPaths.about {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsStandardTableViewCell", for: indexPath)
            cell.textLabel?.text = "settings_about_title".localized()
            cell.detailTextLabel?.text = nil
            cell.textLabel?.font = UIFont.systemFont(ofSize: 17)
            cell.textLabel?.textColor = .black
            return cell
        }
        
        else if indexPath == tableIndexPaths.stopAnimation {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsStandardTableViewCell", for: indexPath)
            cell.textLabel?.text = "settings_stop_title".localized()
            cell.detailTextLabel?.text = nil
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 17)
            cell.textLabel?.textColor = SAConfiguration.sharedInstance.isAnimationPlaying ? .red : .gray
            cell.selectionStyle = SAConfiguration.sharedInstance.isAnimationPlaying ? .blue : .none
            return cell

        }
        
        let cell = UITableViewCell()
        cell.backgroundColor = .clear
        return cell
    }
    
    // MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.cancelClicked()
        
        if indexPath == tableIndexPaths.changeInterval {
            self.pickerView.tag = pickerTags.changeInterval
            self.pickerView.selectRow(SAConfiguration.sharedInstance.changeInterval-1, inComponent: 0, animated: true)
            self.pickerView.reloadAllComponents()
            self.hiddenTF.becomeFirstResponder()
        }
        else if indexPath == tableIndexPaths.colorChooser {
            self.pickerView.tag = pickerTags.colorChooser
            self.pickerView.selectRow(SAConfiguration.sharedInstance.colorMode.rawValue, inComponent: 0, animated: true)
            self.pickerView.reloadAllComponents()
            self.hiddenTF.becomeFirstResponder()
        }
        else if indexPath == tableIndexPaths.about {
            self.showAboutPage()
        }
        else if indexPath == tableIndexPaths.stopAnimation {
            if SAConfiguration.sharedInstance.isAnimationPlaying {
                SAConfiguration.sharedInstance.sendStopAnimationRequest()
                self.tableView.reloadData()
            }
        }
        
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func showAboutPage() {
        self.aboutVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AboutNavigationController") as? UINavigationController
        self.aboutVC!.view.backgroundColor = .clear
        self.dismiss(animated: true, completion: nil)
        if let navVC = UIApplication.shared.keyWindow?.rootViewController {
            navVC.addChild(self.aboutVC!)
            navVC.view.addSubview(self.aboutVC!.view)
            let prevPos = self.aboutVC!.view.frame.origin.y
            self.aboutVC?.view.frame.origin.y = UIScreen.main.bounds.height
            
            UIView.animate(withDuration: 0.5, animations: {
                self.aboutVC!.view.frame.origin.y = prevPos
            }) { (completed) in

            }
        }
    }

    private lazy var changeIntervalArray: [Int] = [1, 2, 3, 4]
    private lazy var colorChooserArray: [String] = ["primary".localized(), "detail".localized(), "background".localized()]

}

extension SettingsTableViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == pickerTags.changeInterval {
            return changeIntervalArray.count
        }
        else if pickerView.tag == pickerTags.colorChooser {
            return colorChooserArray.count
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == pickerTags.changeInterval {
            return String(describing: changeIntervalArray[row])
        }
        else if pickerView.tag == pickerTags.colorChooser {
            return colorChooserArray[row]
        }
        
        return nil
    }
    
    
}

fileprivate class pickerTags {
    private init() { }
    static let changeInterval: Int = 0
    static let colorChooser: Int = 1
}

fileprivate class tableIndexPaths {
    private init() { }
    static let changeInterval: IndexPath = IndexPath(row: 0, section: 0)
    static let colorChooser: IndexPath = IndexPath(row: 1, section: 0)
    static let autoDetect: IndexPath = IndexPath(row: 2, section: 0)
    static let showGuides: IndexPath = IndexPath(row: 3, section: 0)
    static let blackDetection: IndexPath = IndexPath(row: 4, section: 0)
    static let stopAnimation: IndexPath = IndexPath(row: 5, section: 0)
    static let about: IndexPath = IndexPath(row: 0, section: 1)
}
