//
//  CameraAmbientViewController.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 30.09.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit
import AVFoundation
import FirebasePerformance

class CameraAmbientViewController: BaseUIViewController, FrameExtractorDelegate {
    
    @IBOutlet weak var backgroundColorView: UIView!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var secondaryView: UIView!
    @IBOutlet weak var primaryView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var instructionLabel: UILabel!
    @IBOutlet weak var startView: RadialView!
    @IBOutlet weak var startBtn: UIButton!
    
    var frameExtractor: FrameExtractor!
    private let sessionQueue = DispatchQueue(label: "session image queue")
    private var timer: Timer!
    private var currentImage: UIImage!
    private var pointsArray = [UIView]()
    private var performanceTrace: Trace!
    private var frameDetector: FrameDetector!
    
    @IBOutlet weak private var finalImageView: UIImageView!
    @IBOutlet weak private var finalView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareUI()
        let vc = SplashScreen()
        vc.show()
        Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { (timer) in
            vc.stop()
        }
        SAConfiguration.sharedInstance.setConfigurationNotifier {
            if SAConfiguration.sharedInstance.isAnimationPlaying {
                self.startBtnClicked()
            }
        }
        SAConfiguration.sharedInstance.setStopAnimationRequest {
            self.stopAnimation()
        }
        frameExtractor = FrameExtractor()
        frameExtractor.delegate = self
        self.frameDetector = FrameDetector()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.tintColor = .orange
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.frameExtractor.start()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.frameExtractor.stop()
        self.stopAnimation()
    }
    
    override func applicationWillTerminate() {
        if self.performanceTrace != nil { self.performanceTrace.stop() }
    }
    
    func captured(image: UIImage) {
        self.imageView.image = image
        self.currentImage = image
    }
    
    func captureConfidenceLayer(image: UIImage) {
        self.frameDetector.capture(image: image)
        self.frameDetector.pathLayer = self.finalView.layer
    }
    
    func stopAnimation() {
        if self.timer != nil { self.timer.invalidate() }
        if self.performanceTrace != nil {
            self.performanceTrace.stop()
        }
        
        self.startView.alpha = 0.0
        self.startView.isHidden = false
        SAConfiguration.sharedInstance.isAnimationPlaying = false
        UIView.animate(withDuration: 0.5, animations: {
            self.startView.alpha = 1.0
        }, completion: nil)
    }
    
    @objc func drawnStarted(sender: UIPanGestureRecognizer) {
        if sender.state == .began {
            self.gestureStarted(sender)
        }
        else if sender.state == .ended || sender.state == .cancelled {
            self.gestureEnded()
        }
        else {
            self.drawing(gesture: sender)
        }
    }
    
    func gestureStarted(_ gesture: UIPanGestureRecognizer) {
        if self.timer != nil { self.timer.invalidate() }
        SAConfiguration.sharedInstance.isAnimationPlaying = false

        if !self.instructionLabel.isHidden {
            UIView.animate(withDuration: 1.0, animations: {
                self.instructionLabel.alpha = 0.0
            }) { (completed) in
                if completed { self.instructionLabel.isHidden = true }
            }
        }
        
        for point in self.pointsArray { point.removeFromSuperview() }
        self.pointsArray.removeAll()
        previousPoint = nil
    }
    
    private var previousPoint: CGPoint?
    
    func addPoint(to position: CGPoint) {
//        guard let previousPoint = previousPoint else {
//            self.previousPoint = position
//            return
//        }
//
//        let layer = CAShapeLayer()
//
//        let path = UIBezierPath()
//        path.move(to: previousPoint)
//        path.addLine(to: position)
//
//        layer.bounds = self.view.bounds
//        layer.path = path.cgPath
//        layer.position = position
//        layer.strokeColor = UIColor.blue.cgColor
//        layer.fillColor = UIColor.yellow.cgColor
//        self.imageView.layer.addSublayer(layer)
//
//        self.previousPoint = position
        
        let point = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 5))
        point.backgroundColor = .red
        point.layer.cornerRadius = 2.5
        point.clipsToBounds = true
        point.frame.origin = position
        self.pointsArray.append(point)
        self.view.addSubview(point)
        var shouldAdd = false
        var newPoint = point.frame.origin

        if let previousPoint = self.previousPoint {
            let counterX = (previousPoint.x - point.frame.origin.x)
            let counterY = (previousPoint.y - point.frame.origin.y)
            if counterX >= CGFloat(SAConfiguration.sharedInstance.frameSmoothness) || counterX <= -CGFloat(SAConfiguration.sharedInstance.frameSmoothness) {
                newPoint.x = newPoint.x + counterX
                shouldAdd = true
            }
            if counterY >= CGFloat(SAConfiguration.sharedInstance.frameSmoothness) || counterY <= -CGFloat(SAConfiguration.sharedInstance.frameSmoothness) {
                newPoint.y = newPoint.y + counterY
                shouldAdd = true
            }
        }

        self.previousPoint = newPoint
        if shouldAdd { self.addPoint(to: newPoint) }
    }
    
    func drawing(gesture: UIPanGestureRecognizer) {
        let location = gesture.location(in: self.imageView)
        self.addPoint(to: location)
    }
    
    func gestureEnded() {
        if self.startView.isHidden {
            self.startView.isHidden = false
            self.startView.alpha = 0.0
            UIView.animate(withDuration: 1.0, animations: {
                self.startView.alpha = 1.0
            }) { (completed) in
            }
        }
        
        guard let firstPoint = self.pointsArray.first?.frame else { return }
        
        var minX: CGFloat = firstPoint.origin.x
        var minY: CGFloat = firstPoint.origin.y
        var maxX: CGFloat = firstPoint.size.width
        var maxY: CGFloat = firstPoint.size.height
        
        for view in self.pointsArray {
            let point = view.frame.origin
            if point.x <= minX { minX = point.x }
            if point.x >= maxX { maxX = point.x }
            if point.y <= minY { minY = point.y }
            if point.y >= maxY { maxY = point.y }
        }
        let drawnFrame = CGRect(x: minX, y: minY, width: maxX-minX, height: maxY-minY)
        self.finalView.frame = drawnFrame
        let img = self.imageView.image?.cgImage?.cropping(to: drawnFrame)
        self.finalImageView.image = UIImage(cgImage: img!)
        for point in self.pointsArray { point.removeFromSuperview() }
        self.pointsArray.removeAll()
        self.previousPoint = nil
    }
    
    @objc func startTimer() {
        
        guard let imgSize = self.imageView.image?.size else { return }
        let ratio = imgSize.height/self.imageView.frame.height
        let frame = self.finalView.frame
        let rect = CGRect(x: frame.origin.x*ratio, y: frame.origin.y*ratio, width: frame.size.width*ratio, height: frame.size.height*ratio)
        guard let img = self.imageView.image?.cgImage?.cropping(to: rect) else { return }
        
        var uiimg = UIImage(cgImage: img)
        
        self.finalImageView.image = uiimg
        self.captureConfidenceLayer(image: uiimg)
        
        if let confidenceFrame = self.frameDetector.confidenceLayer?.frame {
            let ratedFrame = CGRect(x: confidenceFrame.origin.x*ratio, y: confidenceFrame.origin.y*ratio, width: confidenceFrame.size.width*ratio, height: confidenceFrame.size.height*ratio)
            guard let newImg = self.imageView.image?.cgImage?.cropping(to: CGRect(x: rect.origin.x+ratedFrame.origin.x,
                                                                                  y: rect.origin.y+ratedFrame.origin.y,
                                                                                  width: ratedFrame.width,
                                                                                  height: ratedFrame.height)) else { return }
            uiimg = UIImage(cgImage: newImg)
        }
        
        uiimg.getColors(quality: .lowest) { (color) in
            DispatchQueue.global().async {
                switch SAConfiguration.sharedInstance.colorMode! {
                case .primary:
                    HomeKit.instance.changeLampColors(with: color.primary)
                case .detail:
                    HomeKit.instance.changeLampColors(with: color.detail)
                case .background:
                    HomeKit.instance.changeLampColors(with: color.background)
                }
                
                self.performanceTrace.incrementMetric("next", by: 1)
                
                if SAConfiguration.sharedInstance.isDebugMode {
                    DispatchQueue.main.async {
                        self.primaryView.backgroundColor = color.primary
                        self.secondaryView.backgroundColor = color.secondary
                        self.detailView.backgroundColor = color.detail
                        self.backgroundColorView.backgroundColor = color.background
                    }
                }
                
            }
        }
    }
    
    func prepareUI() {
        let isProduct = !SAConfiguration.sharedInstance.isDebugMode
        self.primaryView.isHidden = isProduct
        self.secondaryView.isHidden = isProduct
        self.backgroundColorView.isHidden = isProduct
        self.detailView.isHidden = isProduct
        self.finalImageView.isHidden = isProduct
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(drawnStarted(sender:)))
        self.imageView.addGestureRecognizer(panGesture)
        self.finalView?.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        self.finalView.backgroundColor = .clear
        self.finalView.layer.borderColor = UIColor.blue.cgColor
        self.finalView.layer.borderWidth = 4.0
        self.view.addSubview(finalView)
        
        let settingsBtn = UIBarButtonItem(image: UIImage(named: "icon_settings"), style: .done, target: self, action: #selector(settingsClicked(sender:)))
        self.navigationItem.setRightBarButton(settingsBtn, animated: true)
        
        self.instructionLabel.text = "camera_vc_instruction_text".localized()
        self.startBtn.setTitle("camera_vc_startbtn_title".localized(), for: .normal)
    }
    
    @objc func settingsClicked(sender: UIBarButtonItem) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SettingsTableViewController") as! SettingsTableViewController
        vc.modalPresentationStyle = .popover
        let popOver = vc.popoverPresentationController
        popOver?.barButtonItem = sender
        popOver?.delegate = self
        vc.view.frame.size.height = vc.tableView.contentSize.height
        self.present(vc, animated: true) {
            
        }
    }
    
    @IBAction func startBtnClicked(_ sender: UIButton? = nil) {
        SAConfiguration.sharedInstance.isAnimationPlaying = true
        if self.timer != nil { self.timer.invalidate() }
        
        UIView.animate(withDuration: 0.5, animations: {
            self.startView.alpha = 0.0
        }) { (completed) in
            if completed { self.startView.isHidden = true }
        }
        self.performanceTrace = Performance.startTrace(name: "Camera Ambient Animation")
        self.timer = Timer.scheduledTimer(timeInterval: TimeInterval(CGFloat(SAConfiguration.sharedInstance.changeInterval)), target: self, selector: #selector(startTimer), userInfo: nil, repeats: true)
    }

    
}

extension CameraAmbientViewController: UIPopoverPresentationControllerDelegate {
    
    func prepareForPopoverPresentation(_ popoverPresentationController: UIPopoverPresentationController) {
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func presentationController(_ controller: UIPresentationController, viewControllerForAdaptivePresentationStyle style: UIModalPresentationStyle) -> UIViewController? {
        let navigationController = UINavigationController(rootViewController: controller.presentedViewController)
        let btnDone = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(dismissVC))
        self.navigationController?.topViewController?.navigationItem.rightBarButtonItem = btnDone
        return navigationController
    }
    
    @objc func dismissVC() {
        if SAConfiguration.sharedInstance.shouldReload {
            self.startBtnClicked()
            SAConfiguration.sharedInstance.reloaded()
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
}
