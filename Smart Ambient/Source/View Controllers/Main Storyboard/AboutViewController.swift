//
//  AboutViewController.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 30.09.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class AboutViewController: BaseUIViewController {

    @IBOutlet weak var visualBackgroundView: UIVisualEffectView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    func prepareUI() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(clicked))
        self.navigationItem.setLeftBarButton(doneBtn, animated: true)
        self.navigationController?.navigationBar.tintColor = .orange
    }
    
    @objc func clicked() {
        UIView.animate(withDuration: 0.5, animations: {
            self.navigationController?.view.frame.origin.y = UIScreen.main.bounds.height
        }) { (completed) in
            self.navigationController?.view.removeFromSuperview()
            self.navigationController?.removeFromParent()
            self.view.removeFromSuperview()
            self.removeFromParent()
        }
    }
        
}

extension AboutViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath == indexPaths.emptyCell {
            return UIScreen.main.bounds.height/2
        }
        else if indexPath == indexPaths.copyrightCell {
            return 220
        }
        else if indexPath == indexPaths.textViewCell {
            return UITableView.automaticDimension
        }
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath == indexPaths.copyrightCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AboutCopyrightTableViewCell", for: indexPath) as! AboutCopyrightTableViewCell
            cell.appVersionLabel.text = "Version \(SAConfiguration.sharedInstance.appLongVersion)"
            return cell
        }
        else if indexPath == indexPaths.textViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AboutTextViewTableViewCell", for: indexPath) as! AboutTextViewTableViewCell
            cell.textView.text = "about_about_text".localized()
            return cell
        }
        
        let cell = UITableViewCell()
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    
}

fileprivate class indexPaths {
    private init() { }
    static let emptyCell = IndexPath(row: 0, section: 0)
    static let copyrightCell = IndexPath(row: 1, section: 0)
    static let textViewCell = IndexPath(row: 2, section: 0)
}
