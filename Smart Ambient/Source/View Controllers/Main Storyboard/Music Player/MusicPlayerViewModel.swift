//
//  MusicPlayerViewModel.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 17.11.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class MusicPlayerViewModel {
    
    public static let sharedInstance = MusicPlayerViewModel()
    public var viewController: MusicPlayerViewController!
    private var isShown = false
    public var lock = false
    
    private init() {
        self.viewController = MusicPlayerViewController()
    }
    
    public func start() {
        NSLog("MusicPlayerViewModel is started to work")
    }
    
    public func shouldShow() {
//        if isShown {
//            return
//        }
//        if lock {
//            return
//        }
//
//        self.isShown = true
//        let rootVC = UIApplication.shared.keyWindow?.rootViewController
//        rootVC?.addChild(self.viewController)
//        self.viewController.view.alpha = 0.0
//        rootVC?.view.addSubview(self.viewController.view)
//        let height = UIScreen.main.bounds.height
//        let musicPlayerHeight = self.viewController.view.frame.size.height
//        self.viewController.view.transform = CGAffineTransform(translationX: 0, y: height)
//        self.viewController.view.alpha = 1.0
//        UIView.animate(withDuration: 0.3, delay: 0.5, options: .curveEaseIn, animations: {
//            var bottomSafeAreaInset: CGFloat = 0
//            if #available(iOS 11.0, *) {
//                bottomSafeAreaInset = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
//            }
//            self.viewController.view.transform = CGAffineTransform(translationX: 0, y: height-musicPlayerHeight-bottomSafeAreaInset-2)
//        }) { (completed) in
//
//        }
    }
    
    public func shouldHide() {
//        if !isShown {
//            return
//        }
//        self.isShown = false
//        guard let rootVC = UIApplication.shared.keyWindow?.rootViewController else {
//            return
//        }
//        for child in rootVC.children {
//            if let childVC = child as? MusicPlayerViewController {
//                childVC.view.removeFromSuperview()
//                childVC.removeFromParent()
//            }
//        }
    }
    
}
