//
//  MusicPlayerViewController.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 5.11.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class MusicPlayerViewController: UIViewController, SpotifyPlayerDelegate {
    
    @IBOutlet weak var playButton: MediaButtonsView!
    @IBOutlet weak var forwardButton: MediaButtonsView!
    @IBOutlet weak var previousButton: MediaButtonsView!
    @IBOutlet weak var trackNameLabel: UILabel!
    @IBOutlet weak var albumNameLabel: UILabel!
    @IBOutlet weak var artcoverImageView: RadialImageView!
    @IBOutlet weak var playerDurationSlider: UISlider!
    @IBOutlet weak var colorView: UIView!
    
    private var isAppeared = false
    
    public var currentPlayer: SPTAppRemotePlayerState?
    
    init() {
        super.init(nibName: "MusicPlayerViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(nibName: "MusicPlayerViewController", bundle: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareUI()
        self.view.isHidden = true
        SpotifyService.sharedInstance.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.transform = CGAffineTransform(translationX: 0, y: 0)
        if currentPlayer == nil {
            self.hide()
        }
        self.isAppeared = true
    }
    
    @objc public func setPlayerPosition() {
        guard let max = currentPlayer?.track.duration else {
            return
        }
        
        SpotifyService.sharedInstance.appRemote.playerAPI?.getPlayerState({ (result, error) in
            if let result = result as? SPTAppRemotePlayerState {
                let pos = result.playbackPosition
                self.playerDurationSlider.value = Float(CGFloat(pos)/CGFloat(max))
            }
        })
    }
    
    public func show() {
        self.view.isHidden = false
        UIView.animate(withDuration: 0.3) {
            self.view.transform = CGAffineTransform(translationX: 0, y: 0)
        }
    }
    
    public func hide() {
        if !isAppeared {
            return
        }
        UIView.animate(withDuration: 0.3) {
            self.view.transform = CGAffineTransform(translationX: 0, y: 300)
        }
    }
    
    @objc public func setMusicPlayer(player: SPTAppRemotePlayerState?) {
        guard let player = player else {
            self.currentPlayer = nil
            self.hide()
            return
        }
        
        self.currentPlayer = player
        self.setPlayerInfo()
        self.show()
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (timer) in
            self.setPlayerPosition()
        }
    }
    
    private func setPlayerInfo() {
        if let player = self.currentPlayer {
            if player.isPaused {
                self.playButton.changeButtonImage(image: UIImage(named: "play"))
            } else {
                self.playButton.changeButtonImage(image: UIImage(named: "pause"))
            }
            self.trackNameLabel.text = player.track.name
            self.albumNameLabel.text = "\(player.track.artist.name) - \(player.track.album.name)"
            SpotifyService.sharedInstance.getImage(track: player.track) { (image) in
                self.artcoverImageView.image = image
            }
        }
    }
    
    private func prepareUI() {
        self.playerDurationSlider.thumbTintColor = .clear
        self.playButton.setActionHandler { (sender) in
            guard let isPaused = self.currentPlayer?.isPaused else {
                return
            }
            if isPaused {
                SpotifyService.sharedInstance.resumeMusic(isSuccessBlock: { (isSucceeded) in
                    if isSucceeded {
                        self.playButton.changeButtonImage(image: UIImage(named: "pause"))
                    }
                })
            } else {
                SpotifyService.sharedInstance.pauseMusic(isSuccessBlock: { (isSucceeded) in
                    if isSucceeded {
                        self.playButton.changeButtonImage(image: UIImage(named: "play"))
                    }
                })
            }
        }
        
        self.forwardButton.setActionHandler { (sender) in
            SpotifyService.sharedInstance.nextMusic(isSuccessBlock: { (isSucceeded) in
                if isSucceeded {
                    
                }
            })
        }
        
        self.previousButton.setActionHandler { (sender) in
            SpotifyService.sharedInstance.previousMusic(isSuccessBlock: { (isSucceeded) in
                if isSucceeded {
                    
                }
            })
        }
    }
    
    func playerStateDidChange(player: SPTAppRemotePlayerState?) {
        self.setMusicPlayer(player: player)
    }
    
    func colorChanged(to color: UIColor?) {
        UIView.animate(withDuration: 0.5) {
            if let color = color {
                self.colorView.backgroundColor = color
            } else {
                self.colorView.backgroundColor = .clear
            }
        }
        
    }

    @IBAction func didSliderValueChanged(_ sender: Any) {
    }
}
