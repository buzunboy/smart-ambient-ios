//
//  LargeMusicViewController.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 14.11.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class LargeMusicViewController: UIViewController, SpotifyPlayerDelegate {
    
    @IBOutlet weak var playButton: MediaButtonsView!
    @IBOutlet weak var forwardButton: MediaButtonsView!
    @IBOutlet weak var previousButton: MediaButtonsView!
    @IBOutlet weak var trackNameLabel: UILabel!
    @IBOutlet weak var albumNameLabel: UILabel!
    @IBOutlet weak var artcoverImageView: RadialImageView!
    @IBOutlet weak var playerDurationSlider: UISlider!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var backgroundArtImageView: UIImageView!
    @IBOutlet weak var providerLogoImageView: RadialImageView!
    @IBOutlet weak var providerTitleLabel: UILabel!
    
    @IBOutlet var connectionAlertView: UIVisualEffectView!
    @IBOutlet weak var CAImageView: UIImageView!
    @IBOutlet weak var CATitleLabel: UILabel!
    
    
    private var isAppeared = true
    
    public var currentPlayer: SPTAppRemotePlayerState?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareUI()
        self.connectionAlertView.frame = self.view.bounds
        self.view.addSubview(self.connectionAlertView)
        self.showAlertView()
//        self.view.isHidden = true
        SpotifyService.sharedInstance.delegate = self
        providerTitleLabel.text = "provider_spotify_title".localized()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        MusicPlayerViewModel.sharedInstance.lock = true
        if #available(iOS 11, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = false
            self.navigationItem.largeTitleDisplayMode = .never
        }
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.tintColor = .white
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if #available(iOS 11, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = true
            self.navigationItem.largeTitleDisplayMode = .always
        }
        
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = nil
        self.navigationController?.navigationBar.tintColor = .blue

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        self.view.transform = CGAffineTransform(translationX: 0, y: 0)
//        if currentPlayer == nil {
//            self.hide()
//        }
//        self.isAppeared = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        MusicPlayerViewModel.sharedInstance.lock = false
    }
    
    @objc public func setPlayerPosition() {
        guard let max = currentPlayer?.track.duration else {
            return
        }
        
        SpotifyService.sharedInstance.appRemote.playerAPI?.getPlayerState({ (result, error) in
            if let result = result as? SPTAppRemotePlayerState {
                let pos = result.playbackPosition
                self.playerDurationSlider.value = Float(CGFloat(pos)/CGFloat(max))
            }
        })
    }
    
    public func showAlertView() {
        if isAppeared {
            return
        }
        self.isAppeared = true
        self.connectionAlertView.isHidden = false
        UIView.animate(withDuration: 0.3) {
            self.connectionAlertView.transform = CGAffineTransform(translationX: 0, y: 0)
        }
    }
    
    public func hideAlertView() {
        if !isAppeared {
            return
        }
        self.isAppeared = false
        UIView.animate(withDuration: 0.3, animations: {
            self.connectionAlertView.transform = CGAffineTransform(translationX: 0, y: UIScreen.main.bounds.height)
        }, completion: { (completed) in
            self.connectionAlertView.isHidden = true
        })
    }
    
    @objc public func setMusicPlayer(player: SPTAppRemotePlayerState?) {
        guard let player = player else {
            self.currentPlayer = nil
            self.showAlertView()
            return
        }
        
        self.currentPlayer = player
        self.setPlayerInfo()
        self.hideAlertView()
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (timer) in
            self.setPlayerPosition()
        }
    }
    
    private func setPlayerInfo() {
        if let player = self.currentPlayer {
            if player.isPaused {
                self.playButton.changeButtonImage(image: UIImage(named: "play"))
            } else {
                self.playButton.changeButtonImage(image: UIImage(named: "pause"))
            }
            self.trackNameLabel.text = player.track.name
            self.albumNameLabel.text = "\(player.track.artist.name) - \(player.track.album.name)"
            SpotifyService.sharedInstance.getImage(track: player.track) { (image) in
                self.artcoverImageView.image = image
                self.backgroundArtImageView.image = image
            }
        }
    }
    
    private func prepareUI() {
        self.playerDurationSlider.thumbTintColor = .clear
        self.playButton.setActionHandler { (sender) in
        }
        
        self.forwardButton.setActionHandler { (sender) in
        }
        
        self.previousButton.setActionHandler { (sender) in
        }
    }
    
    func playerStateDidChange(player: SPTAppRemotePlayerState?) {
        self.setMusicPlayer(player: player)
    }
    
    func colorChanged(to color: UIColor?) {
        UIView.animate(withDuration: 0.5) {
            if let color = color {
                self.colorView.backgroundColor = color
            } else {
                self.colorView.backgroundColor = .clear
            }
        }
        
    }
    
    @IBAction func CAdidClickedBtn(_ sender: Any) {
        SpotifyService.sharedInstance.connectSpotify(with: self)
    }
    
    @IBAction func playBtnClicked(_ sender: Any) {
        self.playButton.clickedAnimate()
        guard let isPaused = self.currentPlayer?.isPaused else {
            return
        }
        if isPaused {
            SpotifyService.sharedInstance.resumeMusic(isSuccessBlock: { (isSucceeded) in
                if isSucceeded {
                    self.playButton.changeButtonImage(image: UIImage(named: "pause"))
                }
            })
        } else {
            SpotifyService.sharedInstance.pauseMusic(isSuccessBlock: { (isSucceeded) in
                if isSucceeded {
                    self.playButton.changeButtonImage(image: UIImage(named: "play"))
                }
            })
        }
    }
    
    @IBAction func previousBtnClicked(_ sender: Any) {
        self.previousButton.clickedAnimate()
        SpotifyService.sharedInstance.previousMusic(isSuccessBlock: { (isSucceeded) in
            if isSucceeded {
                
            }
        })
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        self.forwardButton.clickedAnimate()
        SpotifyService.sharedInstance.nextMusic(isSuccessBlock: { (isSucceeded) in
            if isSucceeded {
                
            }
        })
    }
}
