//
//  HomeViewController.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 14.11.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class HomeViewController: BaseUIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    private final var offsetSpeed: CGFloat = 100
    private final var baseCellHeight: CGFloat = 200
    
    private final var baseCellImageHeight: CGFloat {
        let max = sqrt(pow(baseCellHeight,2) + 4*offsetSpeed*self.tableView.frame.height) - baseCellHeight/2
        return  max + self.baseCellHeight
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UINib(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "baseCell")
        self.navigationItem.title = "home_title".localized()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    // MARK: - Table View Data Source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "baseCell", for: indexPath) as! HomeTableViewCell
        cell.imageViewTopConstraint.constant = self.goOffset(offset: self.tableView.contentOffset.y, cell: cell)
        cell.imageViewHeightConstraint.constant = 250
        
        if indexPath == IndexPath.HomeTable.cameraAmbient {
            cell.backgroundImageView.image = UIImage.getRandomImage(name: "cameraAmbient", upperBound: 7)
            cell.title.text = "home_cameraAmbient_title".localized()
            cell.detailLabel.text = "home_cameraAmbient_subtitle".localized()
        }
        else if indexPath == IndexPath.HomeTable.musicAmbient {
            cell.backgroundImageView.image = UIImage.getRandomImage(name: "musicAmbient", upperBound: 7)
            cell.title.text = "home_musicAmbient_title".localized()
            cell.detailLabel.text = "home_musicAmbient_subtitle".localized()
            cell.providerImageView.image = UIImage(named: "spotify_logo")
        }
        else if indexPath == IndexPath.HomeTable.colorPicker {
            cell.backgroundImageView.image = UIImage.getRandomImage(name: "colorPicker", upperBound: 3)
            cell.title.text = "home_moreWillCome_title".localized()
            cell.detailLabel.text = "home_moreWillCome_subtitle".localized()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.baseCellHeight
    }
    
    // MARK: - Table View Delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath == IndexPath.HomeTable.cameraAmbient {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CameraAmbientViewController")
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath == IndexPath.HomeTable.musicAmbient {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LargeMusicViewController")
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
//        else if indexPath == IndexPath.HomeTable.colorPicker {
//            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ColorPickerViewController")
//            self.navigationController?.pushViewController(vc, animated: true)
//
//        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        for cell in self.tableView.visibleCells {
            if let cell = cell as? HomeTableViewCell {
                cell.mainView.clipsToBounds = true
                cell.imageViewTopConstraint.constant = self.goOffset(offset: self.tableView.contentOffset.y, cell: cell)
            }
        }
    }
    
    // MARK: - Helper Methods
    
    func goOffset(offset: CGFloat, cell: UITableViewCell) -> CGFloat {
        return (offset - cell.frame.origin.y)/baseCellImageHeight*offsetSpeed
    }

}
