//
//  ColorPickerViewController.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 16.11.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class ColorPickerViewController: UIViewController {

    @IBOutlet weak var colorPickerView: ColorPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        colorPickerView.setViewColor(.red)
    }
    
}

class ColorPickerView: CustomColorPicker {
    
    override func brightnessSelected(_ brightness: CGFloat) {
        let previousColor = self.selectedColorView.backgroundColor!
        super.brightnessSelected(brightness)
        let newColor = self.selectedColorView.backgroundColor!
        if newColor != previousColor {
            if !isNextOperationReady {
                return
            }
            
            isNextOperationReady = false
            DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
                self.isNextOperationReady = true
            }
            HomeKit.instance.changeLampColors(with: self.selectedColorView.backgroundColor!)
        }
    }
    
    override func hueAndSaturationSelected(_ hue: CGFloat, saturation: CGFloat) {
        let previousColor = self.selectedColorView.backgroundColor!
        super.hueAndSaturationSelected(hue, saturation: saturation)
        let newColor = self.selectedColorView.backgroundColor!
        if newColor != previousColor {
            if !isNextOperationReady {
                return
            }
            
            isNextOperationReady = false
            DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
                self.isNextOperationReady = true
            }
            HomeKit.instance.changeLampColors(with: self.selectedColorView.backgroundColor!)
        }
    }
    
    public func changeHomeKitColors() {
        
    }
    
    var isNextOperationReady = true

    
    override func touchEnded() {
        HomeKit.instance.changeLampColors(with: self.selectedColorView.backgroundColor!)
        DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
            HomeKit.instance.changeLampColors(with: self.selectedColorView.backgroundColor!)
        }
    }
    
    override func brightnessViewTouchEnded() {
        HomeKit.instance.changeLampColors(with: self.selectedColorView.backgroundColor!)
        DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
            HomeKit.instance.changeLampColors(with: self.selectedColorView.backgroundColor!)
        }
    }
}
