//
//  SplashScreen.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 2.10.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit
import Lottie

class SplashScreen: UIViewController {
    
    @IBOutlet weak var backgroundImageView: RadialImageView!
    @IBOutlet weak var loadingIndicator: SALoadingIndicator!
    
    public var shouldShowLoadingIndicator: Bool = true
    
    init() {
        super.init(nibName: "SplashScreen", bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    final internal override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    final internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    final internal override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    final internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    final internal override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    public func show(shouldAnimate: Bool = false) {
        guard let rootVC = UIApplication.shared.keyWindow?.rootViewController else { return }
        rootVC.present(self, animated: shouldAnimate) {
            if self.shouldShowLoadingIndicator { self.loadingIndicator.startAnimating() }
        }
    }
    
    public func stop() {
        self.loadingIndicator.stopAnimating()
        self.dismiss(animated: false) {
            let animationView = LOTAnimationView(name: "openingAnimation")
            animationView.frame = self.view.bounds
//            animationView.animationSpeed = 0.5
            guard let rootVC = UIApplication.shared.keyWindow?.rootViewController else { return }
            rootVC.view.addSubview(animationView)
            animationView.play { (completed) in
                if completed {
                    animationView.removeFromSuperview()
                }
            }
        }
        
        
    }

}
