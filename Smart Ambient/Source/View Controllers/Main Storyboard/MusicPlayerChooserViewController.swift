//
//  MusicPlayerChooserViewController.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 3.11.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

private let reuseIdentifier = "MusicCollectionCell"

class MusicPlayerChooserViewController: BaseUIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var musicPlayerView: UIView!
    @IBOutlet weak var musicPlayerBottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareUI()
    }
    
    func prepareUI() {
        self.musicPlayerBottomConstraint.constant = 8
        self.navigationItem.title = "select_music_provider".localized()
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
    }

    // MARK: - UICollectionViewDataSource

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return 2
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! MusicCollectionCell
    
        switch indexPath {
        case cellIndexPaths.spotifyCell:
            cell.logoView.image = UIImage(named: "spotify_logo")
            cell.backgroundRadialView.backgroundColor = UIColor(red: 42.0/255.0, green: 42.0/255.0, blue: 42.0/255.0, alpha: 1.0)
            cell.titleLabel.text = "music_chooser_spotify".localized()
            cell.titleLabel.textColor = .white
        case cellIndexPaths.appleMusicCell:
            cell.logoView.image = UIImage(named: "spotify_logo")
            cell.titleLabel.text = "music_chooser_appleMusic".localized()
        default:
            break
        }

        return cell
    }

    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath == cellIndexPaths.spotifyCell {
            self.startSpotifyIntegration()
        }
        else if indexPath == cellIndexPaths.appleMusicCell {
            self.startAppleMusicIntegration()
        }
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.width/2 - 10
        return CGSize(width: width, height: width)
    }
    
    // MARK: - Helper Methods
    
    func startSpotifyIntegration() {
        SpotifyService.sharedInstance.connectSpotify(with: self)
    }
    
    func startAppleMusicIntegration() {
        
    }
    
    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */
    
    /*
     // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
     override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
     return false
     }
     */
 
}

fileprivate enum cellIndexPaths {
    static let spotifyCell = IndexPath(item: 0, section: 0)
    static let appleMusicCell = IndexPath(item: 1, section: 0)
}
