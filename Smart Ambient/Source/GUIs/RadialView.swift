//
//  RadialView.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 30.09.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

@IBDesignable
class RadialView: UIView {
    
    @IBInspectable var radialCorners: Bool = false { didSet { if self.radialCorners { self.radiateCorners() } } }
    @IBInspectable var radius: CGFloat = 0 { didSet { if self.radialCorners { self.radiateCorners() } } }
    
    private func radiateCorners() {
        self.layer.cornerRadius = (radius != 0) ? radius : self.frame.height/2
        self.clipsToBounds = true
    }

}
