//
//  ShadowView.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 5.11.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

@IBDesignable
class ShadowView: UIView {
    
    @IBInspectable var offset: CGSize = CGSize(width: 0, height: 2) {
        didSet {
            self.prepareUI()
        }
    }
    
    @IBInspectable var radius: CGFloat = 2.0 {
        didSet {
            self.prepareUI()
        }
    }
    
    @IBInspectable var opacity: CGFloat = 0.4 {
        didSet {
            self.prepareUI()
        }
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.prepareUI()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.prepareUI()
    }
    
    func prepareUI() {
        self.layer.masksToBounds = false
        self.layer.shadowRadius = self.radius
        self.layer.shadowOpacity = Float(self.opacity)
        self.layer.shadowOffset = self.offset
    }
    
}

