//
//  GradientView.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 14.11.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

@IBDesignable
class GradientView: UIImageView {
    
    @IBInspectable var topPosition: CGPoint = CGPoint(x: 0.5, y: 0.5) {
        didSet {
            self.updatePositions()
        }
    }
    
    @IBInspectable var bottomPosition: CGPoint = CGPoint(x: 0.5, y: 1.0) {
        didSet {
            self.updatePositions()
        }
    }
    private var gradientLayer: CAGradientLayer!
    
    override func draw(_ rect: CGRect) {
        self.prepareUI()
    }
    
    private func updatePositions() {
        if self.gradientLayer != nil {
            self.gradientLayer.startPoint = self.topPosition
            self.gradientLayer.endPoint = self.bottomPosition
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.prepareUI()
    }
    
    private func prepareUI() {
        self.gradientLayer = CAGradientLayer(layer: self.layer)
        self.gradientLayer.frame = self.bounds
        self.gradientLayer.colors = [UIColor.black.cgColor, UIColor.black.withAlphaComponent(0).cgColor]
        self.updatePositions()
        self.gradientLayer.locations = [0,1]
        self.layer.mask = self.gradientLayer
    }
    
}

