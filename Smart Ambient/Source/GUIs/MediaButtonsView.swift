//
//  MediaButtonsView.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 5.11.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

@IBDesignable
class MediaButtonsView: UIView {
    
    typealias ActionHandler = (_ sender: Any)->()
    
    private var actionHandlerBlock: ActionHandler?
    
    @IBInspectable public var buttonImage: UIImage? {
        didSet { self.changeButtonImage(image: self.buttonImage) }
    }
    
    @IBInspectable public var borderColor: UIColor = .white {
        didSet { self.layer.borderColor = self.borderColor.cgColor }
    }
    
    private var innerButton: UIButton!
    private var buttonImageView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialize()
    }
    
    private func initialize() {
        self.backgroundColor = .clear
        self.layer.cornerRadius = self.frame.height/2
        self.clipsToBounds = true
        self.layer.borderColor = self.borderColor.cgColor
        self.layer.borderWidth = 1.0
        let width = self.frame.width
        self.buttonImageView = UIImageView(frame: CGRect(x: 16, y: 16, width: width-32, height: width-32))
        self.buttonImageView.backgroundColor = .clear
        self.innerButton = UIButton(frame: self.bounds)
        self.innerButton.addTarget(self, action: #selector(clickedToButton(sender:)), for: .touchUpInside)
        self.innerButton.isMultipleTouchEnabled = true
        self.changeButtonImage(image: buttonImage)
        self.addSubview(self.buttonImageView)
        self.addSubview(self.innerButton)
        self.bringSubviewToFront(self.innerButton)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.bringSubviewToFront(self.innerButton)
    }
    
    public func changeButtonImage(image: UIImage?) {
        self.buttonImageView.image = image
    }
    
    public func setActionHandler(block: @escaping ActionHandler) {
        self.actionHandlerBlock = block
    }
    
    @objc private func clickedToButton(sender: Any) {
        guard let action = self.actionHandlerBlock else {
            fatalError("setActionHandler: should be called")
        }
        
        self.clickedAnimate()
        action(sender)
    }
    
    public func clickedAnimate() {
        UIView.animate(withDuration: 0.2, delay: 0.0, animations: {
            self.backgroundColor = .white
        }) { (completed) in
            UIView.animate(withDuration: 0.2, animations: {
                self.backgroundColor = .clear
            })
        }
    }
    
}
