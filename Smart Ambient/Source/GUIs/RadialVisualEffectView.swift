//
//  RadialVisualEffectView.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 6.11.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

@IBDesignable
class RadialVisualEffectView: UIVisualEffectView {

    @IBInspectable var radialCorners: CGFloat = 10 { didSet { self.radiateCorners() } }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.radiateCorners()
    }
    
    private func radiateCorners() {
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.radialCorners).cgPath
        self.layer.mask = shapeLayer
    }

}
