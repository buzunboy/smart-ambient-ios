//
//  AboutTextViewTableViewCell.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 1.10.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class AboutTextViewTableViewCell: UITableViewCell {

    @IBOutlet weak var textView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
