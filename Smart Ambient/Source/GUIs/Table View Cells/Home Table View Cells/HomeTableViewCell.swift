//
//  HomeTableViewCell.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 14.11.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var backgroundImageView: GradientView!
    @IBOutlet weak var title: ShadowLabel!
    @IBOutlet weak var detailLabel: ShadowLabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var imageViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var providerImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundImageView.clipsToBounds = false
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
