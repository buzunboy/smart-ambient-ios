//
//  AboutCopyrightTableViewCell.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 1.10.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class AboutCopyrightTableViewCell: UITableViewCell {

    @IBOutlet weak var appLogoImageView: UIImageView!
    @IBOutlet weak var appNameLabel: UILabel!
    @IBOutlet weak var appVersionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
