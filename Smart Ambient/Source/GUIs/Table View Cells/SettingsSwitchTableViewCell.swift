//
//  SettingsSwitchTableViewCell.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 21.10.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class SettingsSwitchTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var switchBtn: UISwitch!
    
    public typealias ValueChangeHandler = (_ value: Bool)->()
    
    private var didChangeHandler: ValueChangeHandler?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func switchValueChanged(handler: @escaping ValueChangeHandler) {
        self.didChangeHandler = handler
    }

    @IBAction func valChanged(_ sender: Any) {
        self.didChangeHandler?(switchBtn.isOn)
    }
}
