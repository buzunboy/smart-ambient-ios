//
//  MusicCollectionCell.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 3.11.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class MusicCollectionCell: UICollectionViewCell {
    @IBOutlet weak var backgroundRadialView: RadialView!
    @IBOutlet weak var logoView: RadialImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
}
