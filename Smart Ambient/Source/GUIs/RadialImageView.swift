//
//  RadialImageView.swift
//  Smart Ambient
//
//  Created by Burak Uzunboy on 2.10.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

@IBDesignable
class RadialImageView: UIImageView {
    
    @IBInspectable var radialCorners: Bool = false { didSet { self.radiateCorners() } }
    @IBInspectable var cornerRadius: CGFloat = 0 { didSet { self.radiateCorners() } }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.radiateCorners()
    }
    
    private func radiateCorners() {
        if self.radialCorners {
            self.layer.cornerRadius = (self.cornerRadius == 0) ? self.frame.height/2 : self.cornerRadius
            self.clipsToBounds = true
        }
    }
    
}

