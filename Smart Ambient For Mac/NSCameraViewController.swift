//
//  NSCameraViewController.swift
//  Smart Ambient For Mac
//
//  Created by Burak Uzunboy on 21.10.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import Cocoa
import AVFoundation

class NSCameraViewController: NSViewController, FrameExtractorDelegate {
    
    @IBOutlet weak var colorsView: NSView!
    @IBOutlet weak var backgroundColorView: NSView!
    @IBOutlet weak var detailView: NSView!
    @IBOutlet weak var secondaryView: NSView!
    @IBOutlet weak var primaryView: NSView!
    @IBOutlet weak var imageView: NSImageView!
    @IBOutlet weak var instructionLabel: NSTextField!
    @IBOutlet weak var startBtn: NSButton!
    
    @IBOutlet weak private var finalImageView: NSImageView!
    @IBOutlet weak private var finalView: NSView!
    
    var frameExtractor: FrameExtractor!
    private var frameDetector: FrameDetector!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.frameExtractor = FrameExtractor()
        self.frameExtractor.delegate = self
        self.frameDetector = FrameDetector()
        self.view.addSubview(finalView)
        SAConfiguration.sharedInstance.setConfigurationNotifier {
            if SAConfiguration.sharedInstance.isAnimationPlaying {
                self.startBtnClicked()
            }
        }
        Timer.scheduledTimer(withTimeInterval: 2, repeats: true) { (timer) in
//            self.startTimer()
        }
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    func captureConfidenceLayer(image: NSImage) {
        self.frameDetector.capture(image: image)
        self.frameDetector.pathLayer = self.finalView.layer
    }
    
    @objc func startTimer() {
        
        guard let imgSize = self.imageView.image?.size else { return }
        let ratio = imgSize.height/self.imageView.frame.height
        let frame = self.finalView.frame
        let rect = CGRect(x: frame.origin.x*ratio, y: frame.origin.y*ratio, width: frame.size.width*ratio, height: frame.size.height*ratio)
        guard let nsImg = self.imageView.image else { return }
        var imgRect = CGRect(x: 0, y: 0, width: nsImg.size.width, height: nsImg.size.height)
        guard let img = self.imageView.image?.cgImage(forProposedRect: &imgRect, context: nil, hints: nil)?.cropping(to: rect) else { return }
        
        var uiimg = NSImage(cgImage: img, size: NSZeroSize)
        
        self.finalImageView.image = uiimg
        self.captureConfidenceLayer(image: uiimg)
        
        if let confidenceFrame = self.frameDetector.confidenceLayer?.frame {
            let ratedFrame = CGRect(x: confidenceFrame.origin.x*ratio, y: confidenceFrame.origin.y*ratio, width: confidenceFrame.size.width*ratio, height: confidenceFrame.size.height*ratio)
            var imgRect = CGRect(x: 0, y: 0, width: nsImg.size.width, height: nsImg.size.height)
            guard let newImg = self.imageView.image?.cgImage(forProposedRect: &imgRect, context: nil, hints: nil)?.cropping(to: CGRect(x: rect.origin.x+ratedFrame.origin.x,
                                                                                  y: rect.origin.y+ratedFrame.origin.y,
                                                                                  width: ratedFrame.width,
                                                                                  height: ratedFrame.height)) else { return }
            uiimg = NSImage(cgImage: newImg, size: NSZeroSize)
        }
        
        uiimg.getColors(quality: .lowest) { (color) in
            DispatchQueue.global().async {
                switch SAConfiguration.sharedInstance.colorMode! {
                case .primary:
                    HueService.sharedInstance.setColorToAllLights(color: color.primary)
                case .detail:
                    HueService.sharedInstance.setColorToAllLights(color: color.detail)
                case .background:
                    HueService.sharedInstance.setColorToAllLights(color: color.background)
                }

//                self.performanceTrace.incrementMetric("next", by: 1)

                if SAConfiguration.sharedInstance.isDebugMode {
                    DispatchQueue.main.async {
                        self.primaryView.layer?.backgroundColor = color.primary.cgColor
                        self.secondaryView.layer?.backgroundColor = color.secondary.cgColor
                        self.detailView.layer?.backgroundColor = color.detail.cgColor
                        self.backgroundColorView.layer?.backgroundColor = color.background.cgColor
                    }
                }

            }
        }
    }
    
    @IBAction func startBtnClicked(_ sender: Any? = nil) {
    }
    
    // MARK: - Frame Extractor Delegate
    func captured(image: NSImage) {
        self.imageView.image = image
        var size = CGRect(x: 0, y: 0, width: 50, height: 50)
        size = self.finalImageView.frame
        self.finalImageView.image = NSImage(cgImage: image.cgImage(forProposedRect: &size, context: nil, hints: nil)!.cropping(to: size)!,
                                            size: NSZeroSize)
    }
    
    // MARK: - Mouse Movement Handlers
    
    private var previousPoint: CGPoint?
    private var pointsArray = [CAShapeLayer]()
    private var positionArray = [CGPoint]()
    
    override func mouseDragged(with event: NSEvent) {
        guard let previousPoint = previousPoint else {
            self.previousPoint = event.locationInWindow
            return
        }

        let shapeLayer = CAShapeLayer()
        shapeLayer.frame = CGRect(x: 0, y: 0,
                                  width: 50, height: 50)
        
        let path = CGMutablePath()
        path.move(to: previousPoint)
        path.addLine(to: event.locationInWindow)

        shapeLayer.path = path
        shapeLayer.lineWidth = 5.0
        shapeLayer.strokeColor = NSColor.red.cgColor
        shapeLayer.fillColor = NSColor.yellow.cgColor
        shapeLayer.fillRule = CAShapeLayerFillRule.evenOdd
        self.view.layer?.addSublayer(shapeLayer)
        self.pointsArray.append(shapeLayer)
        self.previousPoint = event.locationInWindow
        self.positionArray.append(self.previousPoint!)
    }
    
    override func mouseDown(with event: NSEvent) {
        for layer in self.pointsArray {
            layer.removeFromSuperlayer()
        }
        self.previousPoint = nil
        self.pointsArray.removeAll()
        self.positionArray.removeAll()
    }
    
    override func mouseUp(with event: NSEvent) {
        guard let firstPoint = self.positionArray.first else { return }
        
        var minX: CGFloat = firstPoint.x
        var minY: CGFloat = firstPoint.y
        var maxX: CGFloat = firstPoint.x
        var maxY: CGFloat = firstPoint.y
        
        for point in self.positionArray {
            if point.x <= minX { minX = point.x }
            if point.x >= maxX { maxX = point.x }
            if point.y <= minY { minY = point.y }
            if point.y >= maxY { maxY = point.y }
        }
        var drawnFrame = CGRect(x: minX, y: minY, width: maxX-minX, height: maxY-minY)
        
        self.finalView.frame = drawnFrame
        let img = self.imageView.image?.cgImage(forProposedRect: &drawnFrame, context: nil, hints: nil)?.cropping(to: drawnFrame)
        self.finalImageView.image = NSImage(cgImage: img!, size: NSZeroSize)
        for point in self.pointsArray { point.removeFromSuperlayer() }
        self.positionArray.removeAll()
        self.pointsArray.removeAll()
        self.previousPoint = nil
    }

}

