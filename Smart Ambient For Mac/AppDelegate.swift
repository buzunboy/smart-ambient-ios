//
//  AppDelegate.swift
//  Smart Ambient For Mac
//
//  Created by Burak Uzunboy on 21.10.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        HueService.sharedInstance.start()
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

